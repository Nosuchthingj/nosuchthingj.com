var express = require('express')
var app = express()
var haml = require('hamljs');
var fs = require('fs');

app.use(express.static(__dirname + '/public'));

// Setting the url
var url = "http://localhost"
// setting up the / page
app.get('/', function(req, res) {

    // rendering the haml
    var hamlView = fs.readFileSync('public/index.haml', 'utf8');
    res.end(haml.render(hamlView));
});

app.listen(80, function() {
    console.log("started")
})
